﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Drone_DB.DroneDataSetTableAdapters;
using System.IO;

namespace Drone_DB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DroneDataSet DroneData = new DroneDataSet();
        AccumulatorTableAdapter accumulatorTable = new AccumulatorTableAdapter();
        AntennaTableAdapter antennaTable = new AntennaTableAdapter();
        Buy_ComponentTableAdapter buy_ComponentTable = new Buy_ComponentTableAdapter();
        CameraTableAdapter cameraTable = new CameraTableAdapter();
        ClientTableAdapter clientTable = new ClientTableAdapter();
        DoljnostTableAdapter doljnostTable = new DoljnostTableAdapter();
        Flight_controllerTableAdapter flight_ControllerTable = new Flight_controllerTableAdapter();
        MotorTableAdapter motorTable = new MotorTableAdapter();
        MulticopterTableAdapter multicopterTable = new MulticopterTableAdapter();
        PropellerTableAdapter propellerTable = new PropellerTableAdapter();
        RamaTableAdapter ramaTable = new RamaTableAdapter();

        Sell_ComponentTableAdapter sell_ComponentTable = new Sell_ComponentTableAdapter();
        Sell_MulticopterTableAdapter sell_MulticopterTable = new Sell_MulticopterTableAdapter();
        SotrudnikiTableAdapter sotrudnikiTable = new SotrudnikiTableAdapter();
        Speed_ControllerTableAdapter speed_ControllerTable = new Speed_ControllerTableAdapter();
        SupplierTableAdapter supplierTable = new SupplierTableAdapter();
        SupplyTableAdapter supplyTable = new SupplyTableAdapter();
        TransmitterTableAdapter transmitterTable = new TransmitterTableAdapter();
        ZakazTableAdapter zakazTable = new ZakazTableAdapter();

        view_SotrudnikovTableAdapter view_Sotrudnikov = new view_SotrudnikovTableAdapter();
        view_AccumulatorTableAdapter view_Accumulator = new view_AccumulatorTableAdapter();
        view_AntennaTableAdapter view_Antenna = new view_AntennaTableAdapter();
        view_CameraTableAdapter view_Camera = new view_CameraTableAdapter();
        view_FlightControllerTableAdapter view_FlightController = new view_FlightControllerTableAdapter();
        view_MotorTableAdapter view_Motor = new view_MotorTableAdapter();
        view_PropellerTableAdapter view_Propeller = new view_PropellerTableAdapter();
        view_RamaTableAdapter view_Rama = new view_RamaTableAdapter();
        view_SpeedControllerTableAdapter view_SpeedController = new view_SpeedControllerTableAdapter();
        view_TransmitterTableAdapter view_Transmitter = new view_TransmitterTableAdapter();
        view_SupplierTableAdapter view_Supplier = new view_SupplierTableAdapter();
        view_ClientovsTableAdapter view_Clientovs = new view_ClientovsTableAdapter();
        view_MulticopterTableAdapter view_Multicopter = new view_MulticopterTableAdapter();

        view_ZakazTableAdapter view_Zakaz = new view_ZakazTableAdapter();
        view_SellMultiTableAdapter view_SellMulti = new view_SellMultiTableAdapter();
        view_BComponentTableAdapter view_BComponent = new view_BComponentTableAdapter();

        int vibor = 0, level = 0;
        public MainWindow()
        {
            InitializeComponent();

            Grid_start.Visibility = Visibility.Visible;
            Grid_Enter.Visibility = Visibility.Visible;
            GridCollector.Visibility = Visibility.Hidden;
            GridStaff.Visibility = Visibility.Hidden;
            GridManager.Visibility = Visibility.Hidden;
            GridSA.Visibility = Visibility.Hidden;
            Grid_Cl.Visibility = Visibility.Hidden;

            using (FileStream Potok = new FileStream("Users.ur", FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (BinaryWriter FP = new BinaryWriter(Potok))
                {
                    FP.Write("Admin");
                    FP.Write("817224");
                }
            }
        }

        private void B_Vixod_Click(object sender, RoutedEventArgs e)
        {
            Grid_start.Visibility = Visibility.Visible;
            Grid_Enter.Visibility = Visibility.Visible;

            TB_Con_Login.Text = "";
            TB_Con_Password.Text = "";

            GridSA.Visibility = Visibility.Hidden;
            GridSAChange.Visibility = Visibility.Hidden;
            GridCollector.Visibility = Visibility.Hidden;
            Grid_Col_Change.Visibility = Visibility.Hidden;
            GridStaff.Visibility = Visibility.Hidden;
            Grid_St_Change.Visibility = Visibility.Hidden;
            Grid_Cl.Visibility = Visibility.Hidden;
            Grid_Cl_New.Visibility = Visibility.Hidden;
            Grid_Cl_Buy.Visibility = Visibility.Hidden;
            GridManager.Visibility = Visibility.Hidden;
            Grid_M_Comp.Visibility = Visibility.Hidden;



        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                flight_ControllerTable.Fill(DroneData.Flight_controller);
                CB_FC.ItemsSource = DroneData.Tables["Flight_controller"].DefaultView;
                CB_FC.DisplayMemberPath = "FC_Name";
                CB_FC.SelectedValuePath = "ID_FlightCon";

                ramaTable.Fill(DroneData.Rama);
                CB_R.ItemsSource = DroneData.Tables["Rama"].DefaultView;
                CB_R.DisplayMemberPath = "R_Name";
                CB_R.SelectedValuePath = "ID_Rama";

                accumulatorTable.Fill(DroneData.Accumulator);
                CB_AC.ItemsSource = DroneData.Tables["Accumulator"].DefaultView;
                CB_AC.DisplayMemberPath = "A_Name";
                CB_AC.SelectedValuePath = "ID_Accumulator";

                motorTable.Fill(DroneData.Motor);
                CB_M.ItemsSource = DroneData.Tables["Motor"].DefaultView;
                CB_M.DisplayMemberPath = "M_Name";
                CB_M.SelectedValuePath = "ID_Motor";

                speed_ControllerTable.Fill(DroneData.Speed_Controller);
                CB_SC.ItemsSource = DroneData.Tables["Speed_Controller"].DefaultView;
                CB_SC.DisplayMemberPath = "SC_Name";
                CB_SC.SelectedValuePath = "ID_SpeedController";

                propellerTable.Fill(DroneData.Propeller);
                CB_P.ItemsSource = DroneData.Tables["Propeller"].DefaultView;
                CB_P.DisplayMemberPath = "P_Name";
                CB_P.SelectedValuePath = "ID_Propeller";

                transmitterTable.Fill(DroneData.Transmitter);
                CB_T.ItemsSource = DroneData.Tables["Transmitter"].DefaultView;
                CB_T.DisplayMemberPath = "T_Name";
                CB_T.SelectedValuePath = "ID_Transmitter";

                cameraTable.Fill(DroneData.Camera);
                CB_C.ItemsSource = DroneData.Tables["Camera"].DefaultView;
                CB_C.DisplayMemberPath = "C_Name";
                CB_C.SelectedValuePath = "ID_Camera";

                antennaTable.Fill(DroneData.Antenna);
                CB_AN.ItemsSource = DroneData.Tables["Antenna"].DefaultView;
                CB_AN.DisplayMemberPath = "AN_Name";
                CB_AN.SelectedValuePath = "ID_Antenna";

                doljnostTable.Fill(DroneData.Doljnost);
                CB_Sotr_Dolj.ItemsSource = DroneData.Tables["Doljnost"].DefaultView;
                CB_Sotr_Dolj.DisplayMemberPath = "D_Name";
                CB_Sotr_Dolj.SelectedValuePath = "ID_Doljnost";
            }
            catch
            {

            }
        }

        //-----------------------------CONNECTION-------------------------------------

        private void B_EnterToBase_Click(object sender, RoutedEventArgs e)
        {

            switch (new view_SotrTableAdapter().WhoEnter(TB_Con_Login.Text, TB_Con_Password.Text))
            {
                case 1: //SA
                    {
                        level = 1;
                        Grid_start.Visibility = Visibility.Hidden;
                        GridSA.Visibility = Visibility.Visible;
                        GridSAChange.Visibility = Visibility.Hidden;

                        view_Supplier.Fill(DroneData.view_Supplier);
                        dataGrid.ItemsSource = DroneData.view_Supplier.DefaultView;
                        CB_SA_Tables.SelectedIndex = 0;

                        dataGrid.CanUserAddRows = false;
                        dataGrid.CanUserDeleteRows = false;
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        break;
                    }
                case 2: //Collector
                    {
                        level = 2;
                        Grid_start.Visibility = Visibility.Hidden;
                        GridCollector.Visibility = Visibility.Visible;
                        Grid_Col_Change.Visibility = Visibility.Hidden;

                        view_Multicopter.Fill(DroneData.view_Multicopter);
                        dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;


                        dataGrid.CanUserAddRows = false;
                        dataGrid.CanUserDeleteRows = false;
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        break;
                    }
                case 3: //Manager
                    {
                        level = 3;
                        Grid_start.Visibility = Visibility.Hidden;
                        GridManager.Visibility = Visibility.Visible;
                        Grid_M_Comp.Visibility = Visibility.Hidden;

                        view_Accumulator.Fill(DroneData.view_Accumulator);
                        dataGrid.ItemsSource = DroneData.view_Accumulator.DefaultView;
                        CB_M_DataGrid.SelectedIndex = 0;


                        dataGrid.CanUserAddRows = false;
                        dataGrid.CanUserDeleteRows = false;
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        break;
                    }
                case 4: //Staff
                    {
                        level = 4;
                        Grid_start.Visibility = Visibility.Hidden;
                        GridStaff.Visibility = Visibility.Visible;
                        Grid_St_Change.Visibility = Visibility.Hidden;

                        view_Sotrudnikov.Fill(DroneData.view_Sotrudnikov);
                        dataGrid.ItemsSource = DroneData.view_Sotrudnikov.DefaultView;


                        dataGrid.CanUserAddRows = false;
                        dataGrid.CanUserDeleteRows = false;
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        break;
                    }
                default: //Client
                    {
                        if (clientTable.WhoEnterCl(TB_Con_Login.Text, TB_Con_Password.Text) != null && clientTable.Cl_Exist(TB_Con_Login.Text, TB_Con_Password.Text) == true)
                        {
                            Grid_start.Visibility = Visibility.Hidden;
                            Grid_Cl.Visibility = Visibility.Visible;
                            Grid_Cl_New.Visibility = Visibility.Hidden;
                            Grid_Cl_Buy.Visibility = Visibility.Hidden;
                            if (Convert.ToBoolean(clientTable.WhoEnterCl(TB_Con_Login.Text, TB_Con_Password.Text)) == true)
                            {
                                Grid_Cl_New.Visibility = Visibility.Hidden;
                            }
                            else if (Convert.ToBoolean(clientTable.WhoEnterCl(TB_Con_Login.Text, TB_Con_Password.Text)) == false)
                            {
                                Grid_Cl_New.Visibility = Visibility.Visible;
                            }

                            view_Multicopter.Fill(DroneData.view_Multicopter);
                            dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;


                            dataGrid.CanUserAddRows = false;
                            dataGrid.CanUserDeleteRows = false;
                            dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            MessageBox.Show("Неверный логин или пароль");
                        }
                        break;
                    }
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (level == 1)
            {
                DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
                if (selectedDataRow != null)
                {
                    switch (CB_SA_Tables.SelectedIndex)
                    {
                        case 0:
                            {
                                TB_SA_1.Text = selectedDataRow[1].ToString();
                                TB_SA_2.Text = selectedDataRow[2].ToString();
                                TB_SA_3.Text = selectedDataRow[3].ToString();
                                TB_SA_4.Text = selectedDataRow[4].ToString();
                                TB_SA_5.Text = selectedDataRow[5].ToString();
                                TB_SA_6.Text = selectedDataRow[6].ToString();
                                break;
                            }
                        case 1:
                            {
                                TB_SA_1.Text = selectedDataRow[1].ToString();
                                TB_SA_2.Text = selectedDataRow[2].ToString();
                                TB_SA_3.Text = selectedDataRow[3].ToString();
                                TB_SA_4.Text = selectedDataRow[4].ToString();
                                TB_SA_5.Text = selectedDataRow[5].ToString();
                                TB_SA_6.Text = selectedDataRow[6].ToString();
                                TB_SA_7.Text = selectedDataRow[7].ToString();
                                break;
                            }
                    }
                }
            }
            else if (level == 2)
            {

            }
            else if (level == 3)
            {

            }
            else if (level == 4)
            {
                DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
                if (selectedDataRow != null)
                {
                    TB_Sotr_Surname.Text = selectedDataRow[1].ToString();
                    TB_Sotr_Name.Text = selectedDataRow[2].ToString();
                    TB_Sotr_Lastname.Text = selectedDataRow[3].ToString();
                    TB_Sotr_PassSeria.Text = sotrudnikiTable.SerialPassport(Convert.ToInt32(selectedDataRow[0]));
                    TB_Sotr_PassNumber.Text = sotrudnikiTable.NumberPassport(Convert.ToInt32(selectedDataRow[0]));
                    TB_Sotr_Login.Text = sotrudnikiTable.Login(Convert.ToInt32(selectedDataRow[0]));
                    TB_Sotr_Password.Text = sotrudnikiTable.Password(Convert.ToInt32(selectedDataRow[0]));
                    CB_Sotr_Dolj.SelectedValue = sotrudnikiTable.Dolj(Convert.ToInt32(selectedDataRow[0]));
                }
            }
        }

        private void B_ConToBase_Click(object sender, RoutedEventArgs e)
        {
            Grid_Enter.Visibility = Visibility.Hidden;
            Grid_Connection.Visibility = Visibility.Visible;
            TB_Con_Login.Text = "";
            TB_Con_Password.Text = "";
            try
            {
                using (FileStream Potok = new FileStream("Conec.ur", FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader FP = new BinaryReader(Potok))
                    {
                        TB_Con_DataSource.Text = FP.ReadString();
                        TB_Con_InitialCatalog.Text = FP.ReadString();
                        TB_Con_UserID.Text = FP.ReadString();
                        TB_Con_PasswordBase.Text = FP.ReadString();
                    }
                }
            }
            catch
            {

            }

        }

        private void B_Conec_Accept_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                string log, pas;
                using (FileStream Potok = new FileStream("Users.ur", FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader FP = new BinaryReader(Potok))
                    {
                        log = FP.ReadString();
                        pas = FP.ReadString();
                    }
                }
                if (TB_Con_LoginAdmin.Text == log && TB_Con_PasswordAdmin.Text == pas)
                {
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    var connString = config.ConnectionStrings.ConnectionStrings["Drone_DB.Properties.Settings.Drone_CreateConnectionString"];
                    var builder = new SqlConnectionStringBuilder(connString.ConnectionString);
                    builder.DataSource = TB_Con_DataSource.Text;
                    builder.InitialCatalog = TB_Con_InitialCatalog.Text;
                    builder.UserID = TB_Con_UserID.Text;
                    builder.Password = TB_Con_PasswordBase.Text;
                    builder.IntegratedSecurity = false;
                    connString.ConnectionString = builder.ConnectionString;
                    ConfigurationManager.RefreshSection("connectionStrings");
                    config.Save();
                    using (FileStream Potok = new FileStream("Conec.ur", FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        using (BinaryWriter FP = new BinaryWriter(Potok))
                        {
                            FP.Write(TB_Con_DataSource.Text);
                            FP.Write(TB_Con_InitialCatalog.Text);
                            FP.Write(TB_Con_UserID.Text);
                            FP.Write(TB_Con_PasswordBase.Text);
                        }
                    }
                    MessageBox.Show("Успешно подключено");
                    Window_Loaded(sender, e);
                    B_Conec_Canel_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Неверный логин или пароль");
                }
            }
            catch
            {
                MessageBox.Show("Не удалось подключиться");
            }
        }

        private void B_Conec_Canel_Click(object sender, RoutedEventArgs e)
        {
            Grid_Enter.Visibility = Visibility.Visible;
            Grid_Connection.Visibility = Visibility.Hidden;
            TB_Con_DataSource.Text = "";
            TB_Con_PasswordBase.Text = "";
            TB_Con_InitialCatalog.Text = "";
            TB_Con_UserID.Text = "";
            TB_Con_LoginAdmin.Text = "";
            TB_Con_PasswordAdmin.Text = "";
        }
        //-----------------------------\CONNECTION-------------------------------------

        //-----------------------------COLLECTOR-------------------------------------
        private void B_Col_AddDrone_Click(object sender, RoutedEventArgs e)
        {
            Grid_Col_Change.Visibility = Visibility.Visible;
        }

        private void B_Col_DelDrone_Click(object sender, RoutedEventArgs e)
        {
            DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
            if (selectedDataRow != null)
            {
                multicopterTable.Delete_Multi(Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                view_Multicopter.Fill(DroneData.view_Multicopter);
                dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;
                dataGrid.Columns[0].Visibility = Visibility.Hidden;
            }
            else
            {
                MessageBox.Show("Не выбран элемент");
            }
        }

        private void B_Col_Accept_Click(object sender, RoutedEventArgs e)
        {
            if (TB_nameMC.Text != "" && CB_FC.SelectedIndex != -1 && CB_R.SelectedIndex != -1 && CB_AC.SelectedIndex != -1 && CB_M.SelectedIndex != -1 && CB_SC.SelectedIndex != -1 && CB_P.SelectedIndex != -1 && CB_T.SelectedIndex != -1 && CB_C.SelectedIndex != -1 && CB_AN.SelectedIndex != -1)
            {
                if (flight_ControllerTable.MountHole_FC(CB_FC.SelectedIndex) == ramaTable.MountHole(CB_R.SelectedIndex))
                {
                    Grid_Col_Change.Visibility = Visibility.Hidden;
                    multicopterTable.Insert_Multi(TB_nameMC.Text,
                        Convert.ToInt32(sotrudnikiTable.WhoSotr(TB_Con_Login.Text, TB_Con_Password.Text)),
                        Convert.ToInt32(CB_FC.SelectedValue),
                        Convert.ToInt32(CB_R.SelectedValue),
                        Convert.ToInt32(CB_AC.SelectedValue),
                        Convert.ToInt32(CB_M.SelectedValue),
                        Convert.ToInt32(CB_SC.SelectedValue),
                        Convert.ToInt32(CB_P.SelectedValue),
                        Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue))),
                        Convert.ToInt32(CB_T.SelectedValue),
                        Convert.ToInt32(CB_C.SelectedValue),
                        Convert.ToInt32(CB_AN.SelectedValue),
                         Convert.ToDecimal(Convert.ToDecimal(1.2) * flight_ControllerTable.Price(Convert.ToInt32(CB_FC.SelectedValue)) +
                         ramaTable.Price(Convert.ToInt32(CB_R.SelectedValue)) +
                         accumulatorTable.Price(Convert.ToInt32(CB_AC.SelectedValue)) +
                         (motorTable.Price(Convert.ToInt32(CB_M.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                         (speed_ControllerTable.Price(Convert.ToInt32(CB_SC.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                         (propellerTable.Price(Convert.ToInt32(CB_P.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                         transmitterTable.Price(Convert.ToInt32(CB_T.SelectedValue)) +
                         cameraTable.Price(Convert.ToInt32(CB_C.SelectedValue)) +
                         antennaTable.Price(Convert.ToInt32(CB_AN.SelectedValue))),
                        Convert.ToInt32(flight_ControllerTable.Price(Convert.ToInt32(CB_FC.SelectedValue)) +
                        ramaTable.Weight(Convert.ToInt32(CB_R.SelectedValue)) +
                        accumulatorTable.Weight(Convert.ToInt32(CB_AC.SelectedValue)) +
                        (motorTable.Weight(Convert.ToInt32(CB_M.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                        (speed_ControllerTable.Weight(Convert.ToInt32(CB_SC.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                        (propellerTable.Weight(Convert.ToInt32(CB_P.SelectedValue)) * Convert.ToInt32(ramaTable.Count_Rama(Convert.ToInt32(CB_P.SelectedValue)))) +
                        cameraTable.Weight(Convert.ToInt32(CB_C.SelectedValue)))
                        );
                    view_Multicopter.Fill(DroneData.view_Multicopter);
                    dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;
                    dataGrid.Columns[0].Visibility = Visibility.Hidden;
                }
                else
                {
                    MessageBox.Show("монтажные отверстия не совпадает");
                }
            }
            else
            {
                MessageBox.Show("Не выбраны все компоненты или нет названия");
            }
        }

        private void B_Col_Canel_Click(object sender, RoutedEventArgs e)
        {
            Grid_Col_Change.Visibility = Visibility.Hidden;
            CB_FC.SelectedIndex = -1;
            CB_R.SelectedIndex = -1;
            CB_AC.SelectedIndex = -1;
            CB_M.SelectedIndex = -1;
            CB_SC.SelectedIndex = -1;
            CB_P.SelectedIndex = -1;
            CB_T.SelectedIndex = -1;
            CB_C.SelectedIndex = -1;
            CB_AN.SelectedIndex = -1;
        }
        //-----------------------------\COLLECTOR-------------------------------------

        //-----------------------------STAFF-------------------------------------
        private void B_St_AddSotr_Click(object sender, RoutedEventArgs e)
        {
            Grid_St_Change.Visibility = Visibility.Visible;
            TB_Sotr_Surname.Text = "";
            TB_Sotr_Name.Text = "";
            TB_Sotr_Lastname.Text = "";
            TB_Sotr_PassSeria.Text = "";
            TB_Sotr_PassNumber.Text = "";
            TB_Sotr_Login.Text = "";
            TB_Sotr_Password.Text = "";
            CB_Sotr_Dolj.SelectedIndex = -1;
            vibor = 1;
        }

        private void B_St_ChangeSotr_Click(object sender, RoutedEventArgs e)
        {
            Grid_St_Change.Visibility = Visibility.Visible;
            CB_Sotr_Dolj.SelectedIndex = -1;
            vibor = 2;
        }


        private void B_St_DelSotr_Click(object sender, RoutedEventArgs e)
        {
            DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
            if (selectedDataRow != null)
            {
                sotrudnikiTable.Delete_Sotr(Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                view_Sotrudnikov.Fill(DroneData.view_Sotrudnikov);
            }
            else
            {
                MessageBox.Show("Не выбран элемент");
            }
        }

        private void B_St_Accept_Click(object sender, RoutedEventArgs e)
        {

            if (vibor == 1)
            {
                if (TB_Sotr_Surname.Text != "" && TB_Sotr_Name.Text != "" && TB_Sotr_Lastname.Text != "" && TB_Sotr_PassSeria.Text != "" && TB_Sotr_PassNumber.Text != "" && TB_Sotr_Login.Text != "" && TB_Sotr_Password.Text != "" && CB_Sotr_Dolj.SelectedIndex != -1)
                {
                    try
                    {
                        sotrudnikiTable.Insert_Sotr(TB_Sotr_Surname.Text, TB_Sotr_Name.Text, TB_Sotr_Lastname.Text, TB_Sotr_PassSeria.Text, TB_Sotr_PassNumber.Text, TB_Sotr_Login.Text, TB_Sotr_Password.Text, Convert.ToInt32(CB_Sotr_Dolj.SelectedValue));
                        Grid_St_Change.Visibility = Visibility.Hidden;

                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не все строки заполнены");
                }
            }
            if (vibor == 2)
            {
                DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
                if (selectedDataRow != null)
                {
                    if (TB_Sotr_Surname.Text != "" && TB_Sotr_Name.Text != "" && TB_Sotr_Lastname.Text != "" && TB_Sotr_PassSeria.Text != "" && TB_Sotr_PassNumber.Text != "" && TB_Sotr_Login.Text != "" && TB_Sotr_Password.Text != "" && CB_Sotr_Dolj.SelectedIndex != -1)
                    {
                        try
                        {
                            sotrudnikiTable.Update_Sotr(TB_Sotr_Surname.Text, TB_Sotr_Name.Text, TB_Sotr_Lastname.Text, TB_Sotr_PassSeria.Text, TB_Sotr_PassNumber.Text, TB_Sotr_Login.Text, TB_Sotr_Password.Text, Convert.ToInt32(CB_Sotr_Dolj.SelectedValue), Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                            Grid_St_Change.Visibility = Visibility.Hidden;
                        }
                        catch
                        {
                            MessageBox.Show("Одно из полей имеет неверный формат");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не все строки заполнены");
                    }
                }
                else
                {
                    MessageBox.Show("Не выбран элемент для изменения");
                }
            }
            view_Sotrudnikov.Fill(DroneData.view_Sotrudnikov);
        }

        private void B_St_Canel_Click(object sender, RoutedEventArgs e)
        {
            TB_Sotr_Surname.Text = "";
            TB_Sotr_Name.Text = "";
            TB_Sotr_Lastname.Text = "";
            TB_Sotr_PassSeria.Text = "";
            TB_Sotr_PassNumber.Text = "";
            TB_Sotr_Login.Text = "";
            TB_Sotr_Password.Text = "";
            CB_Sotr_Dolj.SelectedIndex = -1;
            Grid_St_Change.Visibility = Visibility.Hidden;
        }

        //-----------------------------\STAFF-------------------------------------

        //-----------------------------MANAGER-------------------------------------
        private void CB_M_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            switch (CB_M_DataGrid.SelectedIndex)
            {
                case 0:
                    {
                        view_Accumulator.Fill(DroneData.view_Accumulator);
                        dataGrid.ItemsSource = DroneData.view_Accumulator.DefaultView;
                        break;
                    }
                case 1:
                    {
                        view_Antenna.Fill(DroneData.view_Antenna);
                        dataGrid.ItemsSource = DroneData.view_Antenna.DefaultView;
                        break;
                    }
                case 2:
                    {
                        view_Camera.Fill(DroneData.view_Camera);
                        dataGrid.ItemsSource = DroneData.view_Camera.DefaultView;
                        break;
                    }
                case 3:
                    {
                        view_FlightController.Fill(DroneData.view_FlightController);
                        dataGrid.ItemsSource = DroneData.view_FlightController.DefaultView;
                        break;
                    }
                case 4:
                    {
                        view_Motor.Fill(DroneData.view_Motor);
                        dataGrid.ItemsSource = DroneData.view_Motor.DefaultView;
                        break;
                    }
                case 5:
                    {
                        view_Propeller.Fill(DroneData.view_Propeller);
                        dataGrid.ItemsSource = DroneData.view_Propeller.DefaultView;
                        break;
                    }
                case 6:
                    {
                        view_Rama.Fill(DroneData.view_Rama);
                        dataGrid.ItemsSource = DroneData.view_Rama.DefaultView;
                        break;
                    }
                case 7:
                    {
                        view_SpeedController.Fill(DroneData.view_SpeedController);
                        dataGrid.ItemsSource = DroneData.view_SpeedController.DefaultView;
                        break;
                    }
                case 8:
                    {
                        view_Transmitter.Fill(DroneData.view_Transmitter);
                        dataGrid.ItemsSource = DroneData.view_Transmitter.DefaultView;
                        break;
                    }
            }
            dataGrid.CanUserAddRows = false;
            dataGrid.CanUserDeleteRows = false;
            dataGrid.Columns[0].Visibility = Visibility.Hidden;
        }

        private void B_M_SellDrone_Click(object sender, RoutedEventArgs e)
        {
            vibor = 1;

            CB_M_DataGrid.Visibility = Visibility.Hidden;
            Grid_M_Comp.Visibility = Visibility.Visible;

            L_M_M_CWorCWW.Visibility = Visibility.Hidden;
            RB_M_M_CW.Visibility = Visibility.Hidden;
            RB_M_M_CWW.Visibility = Visibility.Hidden;


            TB_M_Name.Visibility = Visibility.Hidden;
            Lab_M_Name.Visibility = Visibility.Hidden;
            Lab_M_1.Visibility = Visibility.Hidden;
            Lab_M_2.Visibility = Visibility.Hidden;
            Lab_M_3.Visibility = Visibility.Hidden;
            Lab_M_4.Visibility = Visibility.Hidden;
            Lab_M_5.Visibility = Visibility.Hidden;
            Lab_M_6.Visibility = Visibility.Hidden;
            Lab_M_7.Visibility = Visibility.Hidden;

            TB_M_1.Visibility = Visibility.Hidden;
            TB_M_2.Visibility = Visibility.Hidden;
            TB_M_3.Visibility = Visibility.Hidden;
            TB_M_4.Visibility = Visibility.Hidden;
            TB_M_5.Visibility = Visibility.Hidden;
            TB_M_6.Visibility = Visibility.Hidden;
            TB_M_7.Visibility = Visibility.Hidden;

            L_M_COrS.Visibility = Visibility.Visible;
            L_M_COrS.Content = "Клиент";

            CB_M_COrS.Visibility = Visibility.Visible;

            clientTable.Fill(DroneData.Client);
            CB_M_COrS.ItemsSource = DroneData.Tables["Client"].DefaultView;
            CB_M_COrS.DisplayMemberPath = "Cl_Surname";
            CB_M_COrS.SelectedValuePath = "ID_Client";


            L_M_DOrC.Visibility = Visibility.Visible;
            L_M_DOrC.Content = "Дрон";

            CB_M_Comp.Visibility = Visibility.Visible;

            multicopterTable.Fill(DroneData.Multicopter);
            CB_M_Comp.ItemsSource = DroneData.Tables["Multicopter"].DefaultView;
            CB_M_Comp.DisplayMemberPath = "MC_Name";
            CB_M_Comp.SelectedValuePath = "ID_Multicopter";


            view_Multicopter.Fill(DroneData.view_Multicopter);
            dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;

            dataGrid.CanUserAddRows = false;
            dataGrid.CanUserDeleteRows = false;
            dataGrid.Columns[0].Visibility = Visibility.Hidden;
        }

        private void B_M_BuyComponent_Click(object sender, RoutedEventArgs e)
        {
            vibor = 2;

            CB_M_DataGrid.Visibility = Visibility.Hidden;
            Grid_M_Comp.Visibility = Visibility.Visible;
            CB_M_Comp.Visibility = Visibility.Hidden;
            L_M_DOrC.Visibility = Visibility.Visible;


            L_M_M_CWorCWW.Visibility = Visibility.Hidden;
            RB_M_M_CW.Visibility = Visibility.Hidden;
            RB_M_M_CWW.Visibility = Visibility.Hidden;

            TB_M_Name.Visibility = Visibility.Visible;
            Lab_M_Name.Visibility = Visibility.Visible;
            Lab_M_1.Visibility = Visibility.Hidden;
            Lab_M_2.Visibility = Visibility.Hidden;
            Lab_M_3.Visibility = Visibility.Hidden;
            Lab_M_4.Visibility = Visibility.Hidden;
            Lab_M_5.Visibility = Visibility.Hidden;
            Lab_M_6.Visibility = Visibility.Hidden;
            Lab_M_7.Visibility = Visibility.Hidden;

            TB_M_1.Visibility = Visibility.Hidden;
            TB_M_2.Visibility = Visibility.Hidden;
            TB_M_3.Visibility = Visibility.Hidden;
            TB_M_4.Visibility = Visibility.Hidden;
            TB_M_5.Visibility = Visibility.Hidden;
            TB_M_6.Visibility = Visibility.Hidden;
            TB_M_7.Visibility = Visibility.Hidden;

            L_M_COrS.Visibility = Visibility.Visible;
            L_M_COrS.Content = "Поставщик";

            CB_M_COrS.Visibility = Visibility.Visible;

            supplierTable.Fill(DroneData.Supplier);
            CB_M_COrS.ItemsSource = DroneData.Tables["Supplier"].DefaultView;
            CB_M_COrS.DisplayMemberPath = "Sup_Name";
            CB_M_COrS.SelectedValuePath = "ID_Supplier";

            switch (CB_M_DataGrid.SelectedIndex)
            {
                case 0: // аккумулятор 8
                    {
                        L_M_DOrC.Content = "Аккумулятор";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;
                        Lab_M_5.Visibility = Visibility.Visible;
                        Lab_M_6.Visibility = Visibility.Visible;
                        Lab_M_7.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;
                        TB_M_5.Visibility = Visibility.Visible;
                        TB_M_6.Visibility = Visibility.Visible;
                        TB_M_7.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Емкость";
                        Lab_M_2.Content = "Напряжение";
                        Lab_M_3.Content = "Кол-во ячеек";
                        Lab_M_4.Content = "Вес";
                        Lab_M_5.Content = "Ток";
                        Lab_M_6.Content = "Разъем";
                        Lab_M_7.Content = "Цена";
                        break;
                    }
                case 1: //Антена
                    {
                        L_M_DOrC.Content = "Антенна";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;

                        Lab_M_1.Content = "Тип";
                        Lab_M_2.Content = "Цена";
                        break;
                    }
                case 2: // Камера
                    {
                        L_M_DOrC.Content = "Камера";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Напряжение";
                        Lab_M_2.Content = "Вес";
                        Lab_M_3.Content = "Цена";
                        break;
                    }
                case 3: //Полетный контроллер
                    {
                        L_M_DOrC.Content = "Полетный контроллер";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;
                        Lab_M_5.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;
                        TB_M_5.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Процессор";
                        Lab_M_2.Content = "Вес";
                        Lab_M_3.Content = "Монтажные отверстия";
                        Lab_M_4.Content = "Размер";
                        Lab_M_5.Content = "Цена";
                        break;
                    }
                case 4: //Мотор
                    {
                        L_M_DOrC.Content = "Мотор";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;

                        L_M_M_CWorCWW.Visibility = Visibility.Hidden;
                        RB_M_M_CW.Visibility = Visibility.Hidden;
                        RB_M_M_CWW.Visibility = Visibility.Hidden;

                        Lab_M_1.Content = "Вес";
                        Lab_M_2.Content = "Напряжение";
                        Lab_M_3.Content = "Оборотов в мин";
                        Lab_M_4.Content = "Цена";
                        break;
                    }
                case 5: //Пропеллер
                    {
                        L_M_DOrC.Content = "Пропеллер";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Размер";
                        Lab_M_2.Content = "Вес";
                        Lab_M_3.Content = "Цвет";
                        Lab_M_4.Content = "Цена";
                        break;
                    }
                case 6: // Рама
                    {
                        L_M_DOrC.Content = "Рама";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;
                        Lab_M_5.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;
                        TB_M_5.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Диагональ";
                        Lab_M_2.Content = "Вес";
                        Lab_M_3.Content = "Монтажные отверстия";
                        Lab_M_4.Content = "Кол-во лучей";
                        Lab_M_5.Content = "Цена";
                        break;
                    }
                case 7: //Скоростной контроллер
                    {
                        L_M_DOrC.Content = "Скоростной контроллер";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;
                        Lab_M_3.Visibility = Visibility.Visible;
                        Lab_M_4.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;
                        TB_M_3.Visibility = Visibility.Visible;
                        TB_M_4.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Рабочий ток";
                        Lab_M_2.Content = "Макс. ток";
                        Lab_M_3.Content = "Вес";
                        Lab_M_4.Content = "Цена";
                        break;
                    }
                case 8: //Передатчик
                    {
                        L_M_DOrC.Content = "Передатчик";
                        Lab_M_1.Visibility = Visibility.Visible;
                        Lab_M_2.Visibility = Visibility.Visible;

                        TB_M_1.Visibility = Visibility.Visible;
                        TB_M_2.Visibility = Visibility.Visible;


                        Lab_M_1.Content = "Частота";
                        Lab_M_2.Content = "Цена";
                        break;
                    }
            }
        }

        private void B_M_SellComponent_Click(object sender, RoutedEventArgs e)
        {
            vibor = 3;

            L_M_M_CWorCWW.Visibility = Visibility.Hidden;
            RB_M_M_CW.Visibility = Visibility.Hidden;
            RB_M_M_CWW.Visibility = Visibility.Hidden;

            TB_M_Name.Visibility = Visibility.Hidden;
            Lab_M_Name.Visibility = Visibility.Hidden;
            Lab_M_1.Visibility = Visibility.Hidden;
            Lab_M_2.Visibility = Visibility.Hidden;
            Lab_M_3.Visibility = Visibility.Hidden;
            Lab_M_4.Visibility = Visibility.Hidden;
            Lab_M_5.Visibility = Visibility.Hidden;
            Lab_M_6.Visibility = Visibility.Hidden;
            Lab_M_7.Visibility = Visibility.Hidden;

            TB_M_1.Visibility = Visibility.Hidden;
            TB_M_2.Visibility = Visibility.Hidden;
            TB_M_3.Visibility = Visibility.Hidden;
            TB_M_4.Visibility = Visibility.Hidden;
            TB_M_5.Visibility = Visibility.Hidden;
            TB_M_6.Visibility = Visibility.Hidden;
            TB_M_7.Visibility = Visibility.Hidden;

            L_M_COrS.Visibility = Visibility.Visible;
            L_M_COrS.Content = "Клиент";
            L_M_DOrC.Content = "Компонент";

            Grid_M_Comp.Visibility = Visibility.Visible;

            CB_M_COrS.Visibility = Visibility.Visible;
            clientTable.Fill(DroneData.Client);
            CB_M_COrS.ItemsSource = DroneData.Tables["Client"].DefaultView;
            CB_M_COrS.DisplayMemberPath = "Cl_Surname";
            CB_M_COrS.SelectedValuePath = "ID_Client";

            switch (CB_M_DataGrid.SelectedIndex)
            {
                case 0:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        accumulatorTable.Fill(DroneData.Accumulator);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Accumulator"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "A_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Accumulator";
                        break;
                    }
                case 1:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        antennaTable.Fill(DroneData.Antenna);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Antenna"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "AN_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Antenna";
                        break;
                    }
                case 2:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        cameraTable.Fill(DroneData.Camera);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Camera"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "C_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Camera";
                        break;
                    }
                case 3:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        flight_ControllerTable.Fill(DroneData.Flight_controller);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Flight_controller"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "FC_Name";
                        CB_M_Comp.SelectedValuePath = "ID_FlightCon";
                        break;
                    }
                case 4:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        motorTable.Fill(DroneData.Motor);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Motor"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "M_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Motor";
                        break;
                    }
                case 5:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        propellerTable.Fill(DroneData.Propeller);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Propeller"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "P_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Propeller";
                        break;
                    }
                case 6:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        ramaTable.Fill(DroneData.Rama);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Rama"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "R_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Rama";
                        break;
                    }
                case 7:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        speed_ControllerTable.Fill(DroneData.Speed_Controller);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Speed_Controller"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "SC_Name";
                        CB_M_Comp.SelectedValuePath = "ID_SpeedController";
                        break;
                    }
                case 8:
                    {
                        CB_M_Comp.Visibility = Visibility.Visible;
                        transmitterTable.Fill(DroneData.Transmitter);
                        CB_M_Comp.ItemsSource = DroneData.Tables["Transmitter"].DefaultView;
                        CB_M_Comp.DisplayMemberPath = "T_Name";
                        CB_M_Comp.SelectedValuePath = "ID_Transmitter";
                        break;
                    }
            }
            CB_M_COrS.SelectedItem = 0;
            CB_M_Comp.SelectedItem = 0;
        }

        private void B_M_Accept_Click(object sender, RoutedEventArgs e)
        {
            if (vibor == 1)
            {
                if (CB_M_Comp.SelectedIndex != -1 && CB_M_COrS.SelectedIndex != -1)
                {
                    try
                    {
                        sell_MulticopterTable.Insert_SellMulti(Convert.ToInt32(CB_M_Comp.SelectedValue), Convert.ToInt32(zakazTable.Insert_Zakaz(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(multicopterTable.InfoMC_Price(Convert.ToInt32(CB_M_Comp.SelectedValue))))), 1);

                        Grid_M_Comp.Visibility = Visibility.Hidden;
                        CB_M_DataGrid.Visibility = Visibility.Visible;
                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не выбраны все элементы");
                }
            }
            else if (vibor == 2) // покупка компонента
            {
                switch (CB_M_DataGrid.SelectedIndex)
                {
                    case 0: // аккумулятор 
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "" && TB_M_5.Text != "" && TB_M_6.Text != "" && TB_M_7.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_7.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        accumulatorTable.Insert_Accumulator(
                                            TB_M_Name.Text,
                                            Convert.ToInt32(TB_M_1.Text),
                                            Convert.ToInt32(TB_M_2.Text),
                                            Convert.ToInt32(TB_M_3.Text),
                                            Convert.ToInt32(TB_M_4.Text),
                                            Convert.ToInt32(TB_M_5.Text),
                                            TB_M_6.Text,
                                            Convert.ToInt32(TB_M_7.Text)),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Accumulator.Fill(DroneData.view_Accumulator);
                                        dataGrid.ItemsSource = DroneData.view_Accumulator.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 1: // антенна
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_2.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        antennaTable.Insert_Antenna(
                                            TB_M_Name.Text,
                                            TB_M_1.Text,
                                            Convert.ToDecimal(TB_M_2.Text))
                                        );

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Antenna.Fill(DroneData.view_Antenna);
                                        dataGrid.ItemsSource = DroneData.view_Antenna.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 2: // Камера
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        cameraTable.Insert_Camera(
                                            TB_M_Name.Text,
                                            Convert.ToInt32(TB_M_1.Text),
                                            Convert.ToInt32(TB_M_2.Text),
                                            Convert.ToDecimal(TB_M_3.Text)),
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Camera.Fill(DroneData.view_Camera);
                                        dataGrid.ItemsSource = DroneData.view_Camera.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 3: // Полетный контроллер
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "" && TB_M_5.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        flight_ControllerTable.Insert_FC(
                                            TB_M_Name.Text,
                                            TB_M_1.Text,
                                            Convert.ToInt32(TB_M_2.Text),
                                            Convert.ToDouble(TB_M_3.Text),
                                            TB_M_4.Text,
                                            Convert.ToDecimal(TB_M_5.Text)),

                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_FlightController.Fill(DroneData.view_FlightController);
                                        dataGrid.ItemsSource = DroneData.view_FlightController.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 4: //Мотор
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        motorTable.Insert_Motor(
                                        TB_M_Name.Text,
                                        Convert.ToInt32(TB_M_1.Text),
                                        Convert.ToBoolean(RB_M_M_CW.IsChecked),
                                        Convert.ToInt32(TB_M_2.Text),
                                        Convert.ToInt32(TB_M_3.Text),
                                        Convert.ToDecimal(TB_M_4.Text)),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Motor.Fill(DroneData.view_Motor);
                                        dataGrid.ItemsSource = DroneData.view_Motor.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 5: //Пропеллер
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        propellerTable.Insert_Propeller(
                                            TB_M_Name.Text,
                                            Convert.ToInt32(TB_M_1.Text),
                                            Convert.ToInt32(TB_M_2.Text),
                                            TB_M_3.Text,
                                            Convert.ToDecimal(TB_M_4.Text)),

                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Propeller.Fill(DroneData.view_Propeller);
                                        dataGrid.ItemsSource = DroneData.view_Propeller.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 6: //Рама
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "" && TB_M_5.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        ramaTable.Insert_Rama(
                                            TB_M_Name.Text,
                                            Convert.ToDouble(TB_M_1.Text),
                                            Convert.ToInt32(TB_M_2.Text),
                                            Convert.ToDouble(TB_M_3.Text),
                                            Convert.ToInt32(TB_M_4.Text),
                                            Convert.ToDecimal(TB_M_5.Text)),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Rama.Fill(DroneData.view_Rama);
                                        dataGrid.ItemsSource = DroneData.view_Rama.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 7: //Скоростной контроллер
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "" && TB_M_3.Text != "" && TB_M_4.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        null,
                                        speed_ControllerTable.Insert_SC(
                                            TB_M_Name.Text,
                                            Convert.ToInt32(TB_M_1.Text),
                                            Convert.ToInt32(TB_M_2.Text),
                                            Convert.ToInt32(TB_M_3.Text),
                                            Convert.ToDecimal(TB_M_4.Text)),
                                        null,
                                        null,
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_SpeedController.Fill(DroneData.view_SpeedController);
                                        dataGrid.ItemsSource = DroneData.view_SpeedController.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                    case 8: //Передатчик
                        {
                            if (TB_M_Name.Text != "" && TB_M_1.Text != "" && TB_M_2.Text != "")
                            {
                                if (Convert.ToInt32(TB_M_3.Text) < 0)
                                {
                                    try
                                    {
                                        buy_ComponentTable.Insert_BCom(
                                        supplyTable.InsertQuery(Convert.ToInt32(CB_M_COrS.SelectedValue), Convert.ToDecimal(TB_M_2.Text)),
                                        1,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        transmitterTable.Insert_Transmitter(
                                            TB_M_Name.Text,
                                            Convert.ToDouble(TB_M_1.Text),
                                            Convert.ToDecimal(TB_M_2.Text)),
                                        null,
                                        null);

                                        Grid_M_Comp.Visibility = Visibility.Hidden;
                                        CB_M_DataGrid.Visibility = Visibility.Visible;
                                        view_Transmitter.Fill(DroneData.view_Transmitter);
                                        dataGrid.ItemsSource = DroneData.view_Transmitter.DefaultView;
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Одно из полей имеет неверный формат");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Цена не может быть отрицательной");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не заполнены все поля");
                            }
                            break;
                        }
                }
            }
            else if (vibor == 3)
            {
                if (CB_M_Comp.SelectedIndex != -1 && CB_M_DataGrid.SelectedIndex != -1)
                {
                    switch (CB_M_DataGrid.SelectedIndex)
                    {
                        case 0: // аккумулятор 
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * accumulatorTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);
                                break;
                            }
                        case 1: // антенна
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * antennaTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue)
                                );
                                break;
                            }
                        case 2: // Камера
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * cameraTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null);
                                break;
                            }
                        case 3: // Полетный контроллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * flight_ControllerTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);
                                break;
                            }
                        case 4: //Мотор
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * motorTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null);
                                break;
                            }
                        case 5: //Пропеллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * propellerTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null);
                                break;
                            }
                        case 6: //Рама
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * ramaTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);
                                break;
                            }
                        case 7: //Скоростной контроллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * speed_ControllerTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null,
                                null,
                                null);
                                break;
                            }
                        case 8: //Передатчик
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(CB_M_COrS.SelectedValue),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * transmitterTable.Price(Convert.ToInt32(CB_M_Comp.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_M_Comp.SelectedValue),
                                null,
                                null);
                                break;
                            }
                    }
                }
                else
                {
                    MessageBox.Show("Не выбран элемент");
                }
            }
        }

        private void B_M_Canel_Click(object sender, RoutedEventArgs e)
        {
            Grid_M_Comp.Visibility = Visibility.Hidden;
            CB_M_DataGrid.Visibility = Visibility.Visible;
            CB_M_DataGrid.SelectedIndex = 1;
            CB_M_DataGrid.SelectedIndex = 0;
        }


        private void CB_M_Comp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CB_M_COrS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }



        //-----------------------------\MANAGER-------------------------------------


        //-----------------------------SADIRECTOR-------------------------------------

        private void CB_SA_Tables_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CB_SA_Tables.SelectedIndex)
            {
                case 0:
                    {
                        view_Supplier.Fill(DroneData.view_Supplier);
                        dataGrid.ItemsSource = DroneData.view_Supplier.DefaultView;
                        break;
                    }
                case 1:
                    {
                        view_Clientovs.Fill(DroneData.view_Clientovs);
                        dataGrid.ItemsSource = DroneData.view_Clientovs.DefaultView;
                        break;
                    }
                case 2:
                    {
                        view_Sotrudnikov.Fill(DroneData.view_Sotrudnikov);
                        dataGrid.ItemsSource = DroneData.view_Sotrudnikov.DefaultView;
                        break;
                    }
                case 3:
                    {
                        view_Accumulator.Fill(DroneData.view_Accumulator);
                        dataGrid.ItemsSource = DroneData.view_Accumulator.DefaultView;
                        break;
                    }
                case 4:
                    {
                        view_Antenna.Fill(DroneData.view_Antenna);
                        dataGrid.ItemsSource = DroneData.view_Antenna.DefaultView;
                        break;
                    }
                case 5:
                    {
                        view_Camera.Fill(DroneData.view_Camera);
                        dataGrid.ItemsSource = DroneData.view_Camera.DefaultView;
                        break;
                    }
                case 6:
                    {
                        view_FlightController.Fill(DroneData.view_FlightController);
                        dataGrid.ItemsSource = DroneData.view_FlightController.DefaultView;
                        break;
                    }
                case 7:
                    {
                        view_Motor.Fill(DroneData.view_Motor);
                        dataGrid.ItemsSource = DroneData.view_Motor.DefaultView;
                        break;
                    }
                case 8:
                    {
                        view_Propeller.Fill(DroneData.view_Propeller);
                        dataGrid.ItemsSource = DroneData.view_Propeller.DefaultView;
                        break;
                    }
                case 9:
                    {
                        view_Rama.Fill(DroneData.view_Rama);
                        dataGrid.ItemsSource = DroneData.view_Rama.DefaultView;
                        break;
                    }
                case 10:
                    {
                        view_SpeedController.Fill(DroneData.view_SpeedController);
                        dataGrid.ItemsSource = DroneData.view_SpeedController.DefaultView;
                        break;
                    }
                case 11:
                    {
                        view_Transmitter.Fill(DroneData.view_Transmitter);
                        dataGrid.ItemsSource = DroneData.view_Transmitter.DefaultView;
                        break;
                    }
                case 12:
                    {
                        view_Multicopter.Fill(DroneData.view_Multicopter);
                        dataGrid.ItemsSource = DroneData.view_Multicopter.DefaultView;
                        break;
                    }
                case 13:
                    {
                        view_Zakaz.Fill(DroneData.view_Zakaz);
                        dataGrid.ItemsSource = DroneData.view_Zakaz.DefaultView;
                        break;
                    }
                case 14:
                    {
                        view_SellMulti.Fill(DroneData.view_SellMulti);
                        dataGrid.ItemsSource = DroneData.view_SellMulti.DefaultView;
                        break;
                    }
            }
            dataGrid.CanUserAddRows = false;
            dataGrid.CanUserDeleteRows = false;
            dataGrid.Columns[0].Visibility = Visibility.Hidden;
        }

        private void B_SA_AddSupplier_Click(object sender, RoutedEventArgs e)
        {
            GridSAChange.Visibility = Visibility.Visible;

            TB_SA_1.Visibility = Visibility.Visible;
            TB_SA_2.Visibility = Visibility.Visible;
            TB_SA_3.Visibility = Visibility.Visible;
            TB_SA_4.Visibility = Visibility.Visible;
            TB_SA_5.Visibility = Visibility.Visible;
            TB_SA_6.Visibility = Visibility.Visible;
            TB_SA_7.Visibility = Visibility.Hidden;
            TB_SA_8.Visibility = Visibility.Hidden;
            TB_SA_9.Visibility = Visibility.Hidden;

            TB_SA_1.Text = "";
            TB_SA_2.Text = "";
            TB_SA_3.Text = "";
            TB_SA_4.Text = "";
            TB_SA_5.Text = "";
            TB_SA_6.Text = "";
            TB_SA_7.Text = "";
            TB_SA_8.Text = "";
            TB_SA_9.Text = "";

            L_SA_1.Visibility = Visibility.Visible;
            L_SA_2.Visibility = Visibility.Visible;
            L_SA_3.Visibility = Visibility.Visible;
            L_SA_4.Visibility = Visibility.Visible;
            L_SA_5.Visibility = Visibility.Visible;
            L_SA_6.Visibility = Visibility.Visible;
            L_SA_7.Visibility = Visibility.Hidden;
            L_SA_8.Visibility = Visibility.Hidden;
            L_SA_9.Visibility = Visibility.Hidden;

            L_SA_1.Content = "Название";
            L_SA_2.Content = "Репутация";
            L_SA_3.Content = "Телефон";
            L_SA_4.Content = "Фамилия представителя";
            L_SA_5.Content = "Имя";
            L_SA_6.Content = "Отечство";

            vibor = 1;
        }

        private void B_SA_AddClient_Click(object sender, RoutedEventArgs e)
        {
            GridSAChange.Visibility = Visibility.Visible;
            TB_SA_1.Visibility = Visibility.Visible;
            TB_SA_2.Visibility = Visibility.Visible;
            TB_SA_3.Visibility = Visibility.Visible;
            TB_SA_4.Visibility = Visibility.Visible;
            TB_SA_5.Visibility = Visibility.Visible;
            TB_SA_6.Visibility = Visibility.Visible;
            TB_SA_7.Visibility = Visibility.Visible;
            TB_SA_8.Visibility = Visibility.Visible;
            TB_SA_9.Visibility = Visibility.Visible;

            TB_SA_1.Text = "";
            TB_SA_2.Text = "";
            TB_SA_3.Text = "";
            TB_SA_4.Text = "";
            TB_SA_5.Text = "";
            TB_SA_6.Text = "";
            TB_SA_7.Text = "";
            TB_SA_8.Text = "";
            TB_SA_9.Text = "";

            L_SA_1.Visibility = Visibility.Visible;
            L_SA_2.Visibility = Visibility.Visible;
            L_SA_3.Visibility = Visibility.Visible;
            L_SA_4.Visibility = Visibility.Visible;
            L_SA_5.Visibility = Visibility.Visible;
            L_SA_6.Visibility = Visibility.Visible;
            L_SA_7.Visibility = Visibility.Visible;
            L_SA_8.Visibility = Visibility.Visible;
            L_SA_9.Visibility = Visibility.Visible;

            L_SA_1.Content = "Фамилия клиента";
            L_SA_2.Content = "Имя";
            L_SA_3.Content = "Отчество";
            L_SA_4.Content = "Телефон";
            L_SA_5.Content = "Город";
            L_SA_6.Content = "Улица";
            L_SA_7.Content = "Дом";
            L_SA_8.Content = "Логин";
            L_SA_9.Content = "Пароль";

            vibor = 2;
        }

        private void B_SA_ChangeSupplier_Click(object sender, RoutedEventArgs e)
        {
            GridSAChange.Visibility = Visibility.Visible;

            CB_SA_Tables.SelectedIndex = 0;
            CB_SA_Tables.Visibility = Visibility.Hidden;

            TB_SA_1.Visibility = Visibility.Visible;
            TB_SA_2.Visibility = Visibility.Visible;
            TB_SA_3.Visibility = Visibility.Visible;
            TB_SA_4.Visibility = Visibility.Visible;
            TB_SA_5.Visibility = Visibility.Visible;
            TB_SA_6.Visibility = Visibility.Visible;
            TB_SA_7.Visibility = Visibility.Hidden;
            TB_SA_8.Visibility = Visibility.Hidden;
            TB_SA_9.Visibility = Visibility.Hidden;

            L_SA_1.Visibility = Visibility.Visible;
            L_SA_2.Visibility = Visibility.Visible;
            L_SA_3.Visibility = Visibility.Visible;
            L_SA_4.Visibility = Visibility.Visible;
            L_SA_5.Visibility = Visibility.Visible;
            L_SA_6.Visibility = Visibility.Visible;
            L_SA_7.Visibility = Visibility.Hidden;
            L_SA_8.Visibility = Visibility.Hidden;
            L_SA_9.Visibility = Visibility.Hidden;

            L_SA_1.Content = "Название";
            L_SA_2.Content = "Репутация";
            L_SA_3.Content = "Телефон";
            L_SA_4.Content = "Фамилия представителя";
            L_SA_5.Content = "Имя";
            L_SA_6.Content = "Отечство";

            vibor = 3;
        }

        private void B_SA_ChangeClient_Click(object sender, RoutedEventArgs e)
        {
            GridSAChange.Visibility = Visibility.Visible;

            CB_SA_Tables.SelectedIndex = 1;
            CB_SA_Tables.Visibility = Visibility.Hidden;

            TB_SA_1.Visibility = Visibility.Visible;
            TB_SA_2.Visibility = Visibility.Visible;
            TB_SA_3.Visibility = Visibility.Visible;
            TB_SA_4.Visibility = Visibility.Visible;
            TB_SA_5.Visibility = Visibility.Visible;
            TB_SA_6.Visibility = Visibility.Visible;
            TB_SA_7.Visibility = Visibility.Visible;
            TB_SA_8.Visibility = Visibility.Hidden;
            TB_SA_9.Visibility = Visibility.Hidden;

            L_SA_1.Visibility = Visibility.Visible;
            L_SA_2.Visibility = Visibility.Visible;
            L_SA_3.Visibility = Visibility.Visible;
            L_SA_4.Visibility = Visibility.Visible;
            L_SA_5.Visibility = Visibility.Visible;
            L_SA_6.Visibility = Visibility.Visible;
            L_SA_7.Visibility = Visibility.Visible;
            L_SA_8.Visibility = Visibility.Hidden;
            L_SA_9.Visibility = Visibility.Hidden;

            L_SA_1.Content = "Фамилия клиента";
            L_SA_2.Content = "Имя";
            L_SA_3.Content = "Отчество";
            L_SA_4.Content = "Телефон";
            L_SA_5.Content = "Город";
            L_SA_6.Content = "Улица";
            L_SA_7.Content = "Дом";

            vibor = 4;
        }

        private void B_SA_DelSupplier_Click(object sender, RoutedEventArgs e)
        {
            DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
            if (selectedDataRow != null)
            {
                supplierTable.Delete_Supplier(Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                view_Supplier.Fill(DroneData.view_Supplier);
                dataGrid.ItemsSource = DroneData.view_Supplier.DefaultView;
            }
            else
            {
                MessageBox.Show("Не выбран элемент");
            }

        }


        private void B_SA_DelClient_Click(object sender, RoutedEventArgs e)
        {
            DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
            if (selectedDataRow != null)
            {
                clientTable.Delete_Client(Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                view_Clientovs.Fill(DroneData.view_Clientovs);
                dataGrid.ItemsSource = DroneData.view_Clientovs.DefaultView;
            }
            else
            {
                MessageBox.Show("Не выбран элемент");
            }
        }

        private void B_SA_Accept_Click(object sender, RoutedEventArgs e)
        {

            if (vibor == 1)
            {
                if (TB_SA_1.Text != "" && TB_SA_2.Text != "" && TB_SA_3.Text != "" && TB_SA_4.Text != "" && TB_SA_5.Text != "" && TB_SA_6.Text != "")
                {
                    try
                    {
                        supplierTable.Insert_Supplier(TB_SA_1.Text, Convert.ToInt32(TB_SA_2.Text), TB_SA_3.Text, TB_SA_4.Text, TB_SA_5.Text, TB_SA_6.Text);
                        view_Supplier.Fill(DroneData.view_Supplier);
                        dataGrid.ItemsSource = DroneData.view_Supplier.DefaultView;

                        CB_SA_Tables.Visibility = Visibility.Visible;
                        Grid_St_Change.Visibility = Visibility.Hidden;
                        GridSAChange.Visibility = Visibility.Hidden;
                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не заполнены все поля");
                }
            }
            if (vibor == 2)
            {
                if (TB_SA_1.Text != "" && TB_SA_2.Text != "" && TB_SA_3.Text != "" && TB_SA_4.Text != "" && TB_SA_5.Text != "" && TB_SA_6.Text != "" && TB_SA_7.Text != "" && TB_SA_8.Text != "" && TB_SA_9.Text != "")
                {
                    try
                    {
                        clientTable.Insert_Client(TB_SA_1.Text, TB_SA_2.Text, TB_SA_3.Text, TB_SA_4.Text, TB_SA_5.Text, TB_SA_6.Text, Convert.ToInt32(TB_SA_7.Text), TB_SA_8.Text, TB_SA_9.Text);
                        view_Clientovs.Fill(DroneData.view_Clientovs);
                        dataGrid.ItemsSource = DroneData.view_Clientovs.DefaultView;

                        CB_SA_Tables.Visibility = Visibility.Visible;
                        Grid_St_Change.Visibility = Visibility.Hidden;
                        GridSAChange.Visibility = Visibility.Hidden;

                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не заполнены все поля");
                }
            }
            if (vibor == 3)
            {
                if (TB_SA_1.Text != "" && TB_SA_2.Text != "" && TB_SA_3.Text != "" && TB_SA_4.Text != "" && TB_SA_5.Text != "" && TB_SA_6.Text != "")
                {
                    try
                    {


                        DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
                        if (selectedDataRow != null)
                        {
                            supplierTable.Update_Supplier(TB_SA_1.Text, Convert.ToInt32(TB_SA_2.Text), TB_SA_3.Text, TB_SA_4.Text, TB_SA_5.Text, TB_SA_6.Text, Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                        }
                        view_Supplier.Fill(DroneData.view_Supplier);
                        dataGrid.ItemsSource = DroneData.view_Supplier.DefaultView;

                        CB_SA_Tables.Visibility = Visibility.Visible;
                        Grid_St_Change.Visibility = Visibility.Hidden;
                        GridSAChange.Visibility = Visibility.Hidden;
                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не заполнены все поля");
                }
            }
            if (vibor == 4)
            {
                if (TB_SA_1.Text != "" && TB_SA_2.Text != "" && TB_SA_3.Text != "" && TB_SA_4.Text != "" && TB_SA_5.Text != "" && TB_SA_6.Text != "" && TB_SA_7.Text != "" && TB_SA_8.Text != "" && TB_SA_9.Text != "")
                {
                    try
                    {
                        DataRowView selectedDataRow = (DataRowView)dataGrid.SelectedItem;
                        if (selectedDataRow != null)
                        {
                            clientTable.Update_Client(TB_SA_1.Text, TB_SA_2.Text, TB_SA_3.Text, TB_SA_4.Text, TB_SA_5.Text, TB_SA_6.Text, Convert.ToInt32(TB_SA_7.Text), Convert.ToInt32(selectedDataRow.Row.ItemArray[0]));
                        }
                        view_Clientovs.Fill(DroneData.view_Clientovs);
                        dataGrid.ItemsSource = DroneData.view_Clientovs.DefaultView;

                        CB_SA_Tables.Visibility = Visibility.Visible;
                        Grid_St_Change.Visibility = Visibility.Hidden;
                        GridSAChange.Visibility = Visibility.Hidden;
                    }
                    catch
                    {
                        MessageBox.Show("Одно из полей имеет неверный формат");
                    }
                }
                else
                {
                    MessageBox.Show("Не заполнены все поля");
                }
            }

        }
        private void B_SA_Canel_Click(object sender, RoutedEventArgs e)
        {
            CB_SA_Tables.Visibility = Visibility.Visible;
            GridSAChange.Visibility = Visibility.Hidden;
        }
        //-----------------------------\SADIRECTOR-------------------------------------

        //-----------------------------CLIENT-------------------------------------

        private void B_Cl_NewLP_Click(object sender, RoutedEventArgs e)
        {
            if (TB_Cl_Login.Text != "" && TB_Cl_Password.Text != "")
            {
                try
                {
                    clientTable.Enter_Client(TB_Cl_Login.Text, TB_Cl_Password.Text, Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)));
                    Grid_Cl_New.Visibility = Visibility.Hidden;

                    TB_Con_Login.Text = TB_Cl_Login.Text;
                    TB_Con_Password.Text = TB_Cl_Password.Text;
                }
                catch
                {
                    MessageBox.Show("Одно из полей имеет неверный формат");
                }
            }
        }
        private void B_Cl_Multicopter_Click(object sender, RoutedEventArgs e)
        {
            Grid_Cl_Buy.Visibility = Visibility.Visible;

            CB_Cl_Type.Visibility = Visibility.Hidden;
            CB_Cl_Component.Visibility = Visibility.Hidden;
            CB_Cl_Drone.Visibility = Visibility.Visible;

            L_Cl_type.Visibility = Visibility.Hidden;
            L_Cl_com.Visibility = Visibility.Hidden;
            L_Cl_drone.Visibility = Visibility.Visible;

            L_Cl_Sum.Content = "";

            multicopterTable.Fill(DroneData.Multicopter);
            CB_Cl_Drone.ItemsSource = DroneData.Tables["Multicopter"].DefaultView;
            CB_Cl_Drone.DisplayMemberPath = "MC_Name";
            CB_Cl_Drone.SelectedValuePath = "ID_Multicopter";

            vibor = 1;
        }
        private void CB_Cl_Drone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            L_Cl_Sum.Content = multicopterTable.InfoMC_Price(Convert.ToInt32(CB_Cl_Drone.SelectedValue));
        }
        private void B_Cl_BCom_Click(object sender, RoutedEventArgs e)
        {
            Grid_Cl_Buy.Visibility = Visibility.Visible;

            CB_Cl_Type.Visibility = Visibility.Visible;
            CB_Cl_Component.Visibility = Visibility.Visible;
            CB_Cl_Drone.Visibility = Visibility.Hidden;

            L_Cl_Sum.Content = "";

            L_Cl_type.Visibility = Visibility.Visible;
            L_Cl_com.Visibility = Visibility.Visible;
            L_Cl_drone.Visibility = Visibility.Hidden;

            vibor = 2;
        }
        private void CB_Cl_Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CB_Cl_Type.SelectedIndex)
            {
                case 0:
                    {
                        accumulatorTable.Fill(DroneData.Accumulator);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Accumulator"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "A_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Accumulator";

                        view_Accumulator.Fill(DroneData.view_Accumulator);
                        dataGrid.ItemsSource = DroneData.view_Accumulator.DefaultView;
                        break;
                    }
                case 1:
                    {
                        antennaTable.Fill(DroneData.Antenna);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Antenna"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "AN_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Antenna";

                        view_Antenna.Fill(DroneData.view_Antenna);
                        dataGrid.ItemsSource = DroneData.view_Antenna.DefaultView;
                        break;
                    }
                case 2:
                    {
                        cameraTable.Fill(DroneData.Camera);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Camera"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "C_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Camera";

                        view_Camera.Fill(DroneData.view_Camera);
                        dataGrid.ItemsSource = DroneData.view_Camera.DefaultView;
                        break;
                    }
                case 3:
                    {
                        flight_ControllerTable.Fill(DroneData.Flight_controller);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Flight_controller"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "FC_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_FlightCon";

                        view_FlightController.Fill(DroneData.view_FlightController);
                        dataGrid.ItemsSource = DroneData.view_FlightController.DefaultView;
                        break;
                    }
                case 4:
                    {
                        motorTable.Fill(DroneData.Motor);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Motor"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "M_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Motor";

                        view_Motor.Fill(DroneData.view_Motor);
                        dataGrid.ItemsSource = DroneData.view_Motor.DefaultView;
                        break;
                    }
                case 5:
                    {
                        propellerTable.Fill(DroneData.Propeller);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Propeller"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "P_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Propeller";

                        view_Propeller.Fill(DroneData.view_Propeller);
                        dataGrid.ItemsSource = DroneData.view_Propeller.DefaultView;
                        break;
                    }
                case 6:
                    {
                        ramaTable.Fill(DroneData.Rama);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Rama"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "R_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Rama";

                        view_Rama.Fill(DroneData.view_Rama);
                        dataGrid.ItemsSource = DroneData.view_Rama.DefaultView;
                        break;
                    }
                case 7:
                    {
                        speed_ControllerTable.Fill(DroneData.Speed_Controller);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Speed_Controller"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "SC_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_SpeedController";


                        view_SpeedController.Fill(DroneData.view_SpeedController);
                        dataGrid.ItemsSource = DroneData.view_SpeedController.DefaultView;
                        break;
                    }
                case 8:
                    {
                        transmitterTable.Fill(DroneData.Transmitter);
                        CB_Cl_Component.ItemsSource = DroneData.Tables["Transmitter"].DefaultView;
                        CB_Cl_Component.DisplayMemberPath = "T_Name";
                        CB_Cl_Component.SelectedValuePath = "ID_Transmitter";

                        view_Transmitter.Fill(DroneData.view_Transmitter);
                        dataGrid.ItemsSource = DroneData.view_Transmitter.DefaultView;
                        break;
                    }
            }
        }

        private void CB_Cl_Component_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CB_Cl_Type.SelectedIndex)
            {
                case 0:
                    {
                        L_Cl_Sum.Content = accumulatorTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 1:
                    {
                        L_Cl_Sum.Content = antennaTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 2:
                    {
                        L_Cl_Sum.Content = cameraTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 3:
                    {
                        L_Cl_Sum.Content = flight_ControllerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 4:
                    {
                        L_Cl_Sum.Content = motorTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 5:
                    {
                        L_Cl_Sum.Content = propellerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 6:
                    {
                        L_Cl_Sum.Content = ramaTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 7:
                    {
                        L_Cl_Sum.Content = speed_ControllerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
                case 8:
                    {
                        L_Cl_Sum.Content = transmitterTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue));
                        break;
                    }
            }
        }

        private void B_Cl_Accept_Click(object sender, RoutedEventArgs e)
        {
            if (vibor == 1)
            {
                if (CB_Cl_Drone.SelectedIndex != -1)
                {
                    sell_MulticopterTable.Insert_SellMulti(Convert.ToInt32(CB_Cl_Drone.SelectedValue), Convert.ToInt32(zakazTable.Insert_Zakaz(Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)), Convert.ToDecimal(multicopterTable.InfoMC_Price(Convert.ToInt32(CB_Cl_Drone.SelectedValue))))), 1);
                    Grid_Cl_Buy.Visibility = Visibility.Hidden;
                    L_Cl_Sum.Content = "";

                    CB_Cl_Component.SelectedIndex = -1;
                    CB_Cl_Drone.SelectedIndex = -1;
                    CB_Cl_Type.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент");
                }
            }
            else if (vibor == 2)
            {
                if (CB_Cl_Type.SelectedIndex != -1 && CB_Cl_Component.SelectedIndex != -1)
                {
                    switch (CB_Cl_Type.SelectedIndex)
                    {
                        case 0: // аккумулятор 
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * accumulatorTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 1: // антенна
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * antennaTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue));

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 2: // Камера
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * cameraTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 3: // Полетный контроллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * flight_ControllerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 4: //Мотор
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * motorTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 5: //Пропеллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * propellerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 6: //Рама
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * ramaTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 7: //Скоростной контроллер
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * speed_ControllerTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null,
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                        case 8: //Передатчик
                            {
                                sell_ComponentTable.Insert_SCom(
                                    zakazTable.Insert_Zakaz(
                                        Convert.ToInt32(clientTable.ID_Client(TB_Con_Login.Text, TB_Con_Password.Text)),
                                        Convert.ToDecimal(Convert.ToDecimal(1.3) * transmitterTable.Price(Convert.ToInt32(CB_Cl_Component.SelectedValue)))),
                                1,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                Convert.ToInt32(CB_Cl_Component.SelectedValue),
                                null,
                                null);

                                Grid_Cl_Buy.Visibility = Visibility.Hidden;
                                L_Cl_Sum.Content = "";

                                CB_Cl_Component.SelectedIndex = -1;
                                CB_Cl_Drone.SelectedIndex = -1;
                                CB_Cl_Type.SelectedIndex = -1;
                                break;
                            }
                    }
                }
                else
                {
                    MessageBox.Show("Не выбраны все элементы");
                }
            }
        }



        private void B_Cl_Canel_Click(object sender, RoutedEventArgs e)
        {
            Grid_Cl_Buy.Visibility = Visibility.Hidden;
            L_Cl_Sum.Content = "";

            CB_Cl_Component.SelectedIndex = -1;
            CB_Cl_Drone.SelectedIndex = -1;
            CB_Cl_Type.SelectedIndex = -1;
        }

        //-----------------------------\CLIENT-------------------------------------
    }
}
